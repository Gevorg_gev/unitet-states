﻿using System;
using System.Linq;

namespace Exam
{
    class Program
    {
        static void Main(string[] args)
        {
            Students [] s = new Students[3];
            s[0] = new Students(1,"John",750);
            s[1] = new Students(2,"Alice",720);
            s[3] = new Students(3,"Jason",760);

            //Console.Write("Enter your choice please - ");
            //int n = int.Parse(Console.ReadLine());

            var Result = from X in s group X by X;

            foreach (var item in s)
            {
                Console.Write(item + "  ");
            }
        }
    }
}
