﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam
{
    public class Students
    {
        public long Id { get; set; }
        public string Name { get; set; } 
        public int Grade { get; set; }

        public Students(long Id,string Name, int Grade) {

            this.Id = Id;
            this.Name = Name;
            this.Grade = Grade; 
        }
    }
}
